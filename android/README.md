CheckStyles info:
http://checkstyle.sourceforge.net/
Avilable checks listed on:
http://checkstyle.sourceforge.net/availablechecks.html

------

Kaldor style settings for Android Studio 1.0.0:

* Link the style to your personal preferences:
`ln codeStyleSettings.xml ~/Library/Preferences/AndroidStudio/codestyles/Kaldor.xml`

If the directory doesn't exist, create it.

Note: with my latest IDE update the path changed to:
`~/Library/Preferences/AndroidStudioPreview1.2/codestyles/Kaldor.xml`
Thanks Google.

* In Android Studio choose the coding style
Android Studio | Preferences | Code Style
Choose "Kaldor" as your scheme

* Apply the styling
Code | Reformat code (⌥⌘L)

